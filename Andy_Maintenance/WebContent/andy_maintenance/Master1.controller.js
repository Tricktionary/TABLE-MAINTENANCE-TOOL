/*
 * Global Variables for the table
 */
var view;
var keys = [];
var property = [];
var sServiceUrl ;
var extra;
var globalint;
sap.ui.controller("andy_maintenance.Master1", {

/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
* @memberOf andy_maintenance.Master1
*/
	onInit: function() {
		globalint = 0;
	},
	BindOpen:function(){
		sap.ui.getCore().byId("BindDialog").open();       
	},
	
	Bind:function(){
		//var sServiceUrl = "/sap/opu/odata/SAP/ZTEST_MG_LM2_SRV";
		//    /ResultSet
		
		//var sServiceUrl = "/sap/opu/odata/SAP/ZMM_EMP_SRV";
		//    /EmployeeSet
		keys = [];
		property = [];
		sServiceUrl = "";
		extra = "";
		sServiceUrl = sap.ui.getCore().byId("URL").getValue();
		extra = sap.ui.getCore().byId("Extra").getValue()
       
		//console.log(this);
		var oModel = new sap.ui.model.odata.ODataModel(sServiceUrl,true);
        var oJsonModel = new sap.ui.model.json.JSONModel();
        view = this;
   
        var labels= [];
        
 
        oModel.attachMetadataLoaded(null,function(){
           globalint++;
           var oTable = sap.ui.getCore().byId("Table");
           var oTemplate = sap.ui.getCore().byId("oTemplate");
           var oDialogue = sap.ui.getCore().byId("Dialog");
           sap.ui.getCore().setModel();										//CLEAR MODEL
           oTable.unbindItems();
           oTable.removeAllColumns();
           oDialogue.removeAllButtons();
           oDialogue.removeAllContent();
           oTemplate.destroyCells();
           
           var oMetadata = oModel.getServiceMetadata();
           
           var keyArray = oMetadata.dataServices.schema[0].entityType[0].key.propertyRef;					//Key Array
           
           //console.log(keyArray);
           //console.log(keyArray.propertyRef);
           for (var i = 0 ; i < keyArray.length;i++){											//Loading Keys
        	   keys[i] = keyArray[i].name;
           }
            
           //console.log(keys);
           var propertyArray = oMetadata.dataServices.schema[0].entityType[0].property;			//Shorthands the Properties array
           
 
           
           for (var i = 0; i < propertyArray.length ; i++){												    //Pulls Property Values
        	   //console.log(propertyArray[i].extensions[0].value);
        	   property[i] = propertyArray[i].name;												//THIS LINE IS USELESS
        	   oTable.addColumn(new sap.m.Column({
             	  header: new sap.m.Label({
             		 text:propertyArray[i].extensions[0].value,
             	  }),
                }));
        	   oTemplate.addCell(new sap.m.Label({
                   text:"{"+property[i]+"}",
               }));
           
        	   oDialogue.addContent(new sap.m.Label({text:propertyArray[i].extensions[0].value,}));
               //console.log(property[i]+globalint);
        	   oDialogue.addContent(new sap.m.Input(property[i]+globalint,{
            	   maxLength: parseInt(propertyArray[i].maxLength),
               }));
           }
           oTable.bindItems(extra,oTemplate, null,null);
           
           oDialogue.addButton(sap.ui.getCore().byId("Update"));
           oDialogue.addButton(sap.ui.getCore().byId("Delete"));
           oDialogue.addButton(sap.ui.getCore().byId("Cancel"));
           oDialogue.addButton(sap.ui.getCore().byId("Save"));
           sap.ui.getCore().byId("URL").setValue("");
           sap.ui.getCore().byId("Extra").setValue("")
           //Read The data values
           oModel.read( extra+"?",null,null,true,function(oData,response){
           	oJsonModel.setData(oData);
           });
           sap.ui.getCore().setModel(oModel);										//Setting Model
         
        },null);
        
         
        sap.ui.getCore().byId("BindDialog").close();
	},
	
	ItemPress: function(evt) {
		sap.ui.getCore().byId("Dialog").open();                    
        sap.ui.getCore().byId("Update").setVisible(true);
        sap.ui.getCore().byId("Delete").setVisible(true);
        sap.ui.getCore().byId("Save").setVisible(false);
        
        var oSelectedItem = evt.getParameter("listItem");
        var valueArray =[];
        
        //Pulling the values of the selected item
        for(var i = 0; i <property.length; i++){
        	valueArray[i] = oSelectedItem.getBindingContext().getProperty(property[i]);
        }
        //console.log(valueArray);
        //Setting the values in the oDiague
        for(var i = 0; i < property.length; i++){
        	sap.ui.getCore().byId(property[i]+globalint).setValue(valueArray[i]);
        }
        for(var i = 0 ; i< keys.length ; i++){
        	sap.ui.getCore().byId(keys[i]+globalint).setEnabled(false);
        }
    },

    NewEntry: function() {
    	
    	sap.ui.getCore().byId("Dialog").open(); 
    	sap.ui.getCore().byId("Save").setVisible(true);
        sap.ui.getCore().byId("Update").setVisible(false);
        sap.ui.getCore().byId("Delete").setVisible(false);
        for(var i = 0; i < property.length; i++){
        	sap.ui.getCore().byId(property[i]+globalint).setValue("");
        }
        for(var i = 0 ; i< keys.length ; i++){
        	sap.ui.getCore().byId(keys[i]+globalint).setEnabled(true);
        }
        
 
   },      
   
   //CREATE
   Save: function() {
	   
	   var oModel = sap.ui.getCore().getModel();
	   var oEntry = {};
 
	   for(var i = 0 ; i <property.length ;i++){
		   oEntry[property[i]] = sap.ui.getCore().byId(property[i]+globalint).getValue();
	   }
	   //console.log(oEntry);
	   
	   oModel.create(extra, oEntry, null, function(){
	       	sap.m.MessageToast.show("Sucess", {});
	       		sap.ui.getCore().byId("Dialog").close();
	           },function(){
	        alert("Error!");
	   });
	                                            
    },
    
    //UPDATE
    Update: function() {
    	var oModel = sap.ui.getCore().getModel();
        var oEntry = {};
        
        for(var i = 0 ; i <property.length ;i++){
 		   oEntry[property[i]] = sap.ui.getCore().byId(property[i]+globalint).getValue();
 	   	}
 
        
        var keyString = "";
        
        for( var i = 0; i< keys.length; i++){
         	keyString += sap.ui.getCore().byId(keys[i]+globalint).getValue();
         } 
        
        oModel.update(extra+"('" + keyString + "')", oEntry, null, function(){
      	  	sap.m.MessageToast.show("Sucess", {});
      	  	sap.ui.getCore().byId("Dialog").close();
              },function(){
              	alert("Error!");
        }); 
     },
     
     //Delete Action                                           
     Delete: function() {
    	 var oModel = sap.ui.getCore().getModel();
    	 var oEntry = {};
    	 //oEntry[key]= sap.ui.getCore().byId(key+globalint).getValue();
    	 var keyString ="";
         
         for( var i = 0; i< keys.length; i++){
         	keyString += sap.ui.getCore().byId(keys[i]+globalint).getValue();
         } 
    	 
         oModel.remove(extra+"('" + keyString + "')", oEntry, null, function(){
        	 sap.ui.getCore().byId("Dialog").close();	
        	 sap.m.MessageToast.show("Sucess", {});
         	},function(){
         		alert("Error!");
         	}
         ); 
    },
    //Cancel Action                         
    Cancel:function() {
    	sap.ui.getCore().byId("Dialog").close();
    },
    
    //Close Binding Dialogue
    BindCancel:function() {
    	sap.ui.getCore().byId("BindDialog").close();
    },

});