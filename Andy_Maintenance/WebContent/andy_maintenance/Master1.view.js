sap.ui.jsview("andy_maintenance.Master1", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf andy_maintenance.Master1
	*/ 
	getControllerName : function() {
		return "andy_maintenance.Master1";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	* @memberOf andy_maintenance.Master1
	*/ 
	createContent : function(oController) {
		
		//Update Button
		var oBtnUpd = new sap.m.Button("Update",{
			text: "Update",
			tap: [oController.Update , oController]
		});
		
		//Delete Button
		var oBtnDel = new sap.m.Button("Delete", {
             text: "Delete",
             tap: [ oController.Delete, oController ]
        });
		
		//Cancel Button
		var oBtnCan = new sap.m.Button("Cancel", {
             text: "Close",
             tap: [ oController.Cancel, oController ]
        });
		
		//Save Button
		var oBtnSav = new sap.m.Button("Save", {
             text: "Save",
             tap: [ oController.Save, oController ]
        });
		
		//Create Prompt Button
		var oBtnSub = new sap.m.Button("Submit",{
             text: "Create",
             press: oController.NewEntry,
        });
		
		//BIND BUTTON
		var oBtnBind = new sap.m.Button("Bind",{
			text:"Bind",
			press: oController.BindOpen,
		});
		var bindSubmit = new sap.m.Button("BindSubmit",{
			text:"Submit",
			press: oController.Bind,
		});
		var bindCancel = new sap.m.Button("BindCancel",{
			text:"Cancel",
			press: oController.BindCancel,
		});
		
		//Dialogue Bind
		var oDialog = new sap.m.Dialog("BindDialog",{
            title:"Bind",
            modal: true,
            contentWidth:"1em",
            content:[
           	 new sap.m.Label({text:"URL"}),
           	 new sap.m.Input("URL",{ }),
	            
           	 new sap.m.Label({text:"Extra"}),
           	 new sap.m.Input("Extra",{ })           
            ],
            buttons:[
            	bindSubmit,
            	bindCancel
            ]
		});
		
		//Dialogue
		var oDialog = new sap.m.Dialog("Dialog",{
            title:"Add/Modify",
            modal: true,
            contentWidth:"1em",
            buttons:[
            	oBtnCan
            ]
		});
		
		//Table
		var oTable = new sap.m.Table("Table",{
			itemPress : [ oController.ItemPress,oController ],
		});
		
		//Template
		var template = new sap.m.ColumnListItem({
			id: "oTemplate",
		    type: "Navigation",
		    visible: true,
		});
		
 
		return new sap.m.Page({
			title: "Table Maintenance Tool",
			content: [
				oBtnSub,
				oBtnBind,
				oTable
			]
		});
	}

});